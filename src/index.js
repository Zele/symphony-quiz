import React from 'react'
import ReactDOM from 'react-dom'
import './index.css'
import App from './main/App'
import { BrowserRouter as Router } from 'react-router-dom'

import * as serviceWorker from './serviceWorker'

const routedApp =
    <Router>
        <App />
    </Router>

ReactDOM.render(routedApp, document.getElementById('root'))

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
serviceWorker.unregister()
