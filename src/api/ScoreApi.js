// @flow

import axios from 'axios'

type Score = {
    nickname: string,
    correctAnswers: number,
    timeUsedMs: number,
    email: string
}

class ScoreApi {
    submitScore = (score: Score) => {
        const scorePayload = {
            nickname: score.nickname,
            correct_count: score.correctAnswers,
            time_used_ms: score.timeUsedMs,
            email: score.email
        }

        axios.post(
            `${process.env.REACT_APP_BACKEND_URL}/user_scores`,
            scorePayload
        )
    }
}

export default ScoreApi
