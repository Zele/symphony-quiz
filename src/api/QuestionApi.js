// @flow

import validate from './questionValidator'

class QuestionApi {
    cachedData = []

    fetchQuestions = async () => {
        if (this.cachedData.length === 0) {
            const response = await fetch('questions.json')
            const questions = await response.json()
            if (validate(questions)) {
                this.cachedData = questions
            } else {
                throw Error('Data is corrupt or in invalid format! Please refer to JSON schema.')
            }
        }
        return this.cachedData
    }

    getRandomQuestions = async (count: number) => {
        const questions = await this.fetchQuestions()
        // Shuffle array
        const shuffled = questions.sort(() => 0.5 - Math.random());

        // Get sub-array of first n elements after shuffled
        return shuffled.slice(0, count);
    }

}

export default QuestionApi

