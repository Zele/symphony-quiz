// @flow

import Ajv from 'ajv'
import questionSchema from './questionSchema'

const ajv = new Ajv()

const validator = ajv.compile(questionSchema)

export default (data: Object | Array<Object>): Boolean => validator(data)
