// @flow
import * as React from 'react';
import QuestionApi from '../api/QuestionApi'
import QuizQuestion from '../components/quiz/QuizQuestion'
import { PushSpinner } from 'react-spinners-kit'
import Timer from "./Timer/Timer";

import './Quiz.css'

type PropsType = {
    incrementCorrectAnswerCount: Function,
    signalTimerFinished: Function,
    recordTime: Function,
    validateUserHasLoggedIn: Function,
}

type StateType = {
    cnt: number,
    questions: Array<QuizQuestionType>,
    questionTotal: number,
    loadingQuestions: boolean
};

type QuizQuestionType = {
    question: string,
    answers: Array<AnswerType>,
    correctAnswer: number
}

export type AnswerType = {
    id: number,
    content: number
}

const api = new QuestionApi()

class Quiz extends React.Component<PropsType, StateType> {
    constructor(props) {
        super(props)
        this.state = {
            cnt: 0,
            questions: [],
            questionTotal: process.env.REACT_APP_QUESTION_NUM,
            loadingQuestions: true
        }
    }

    async componentDidMount() {
        const { questionTotal } = this.state
        const questions = await api.getRandomQuestions(questionTotal)

        const recalculateQuestionCount = Math.min(process.env.REACT_APP_QUESTION_NUM, questions.length)

        this.setState({ questions, loadingQuestions: false, questionTotal: recalculateQuestionCount })
    }

    nextAnswer = selectedAnswered => {
        const { questionTotal, cnt, questions } = this.state
        const { incrementCorrectAnswerCount, signalTimerFinished } = this.props

        if (cnt < questionTotal - 1) {
            if (selectedAnswered === questions[cnt].correctAnswer) {
                incrementCorrectAnswerCount()
            }
            this.setState(prevState => ({ cnt: prevState.cnt + 1 }))
        } else {
            signalTimerFinished()
        }
    }


    render() {
        const { cnt, questions, questionTotal, loadingQuestions } = this.state
        const { signalTimerFinished, recordTime, validateUserHasLoggedIn } = this.props

        validateUserHasLoggedIn()

        if (loadingQuestions) {
            return <PushSpinner />
        }

        return (
            <div className="quiz-wrapper">
                <div className="quiz-info">
                    <div className="quiz-question-number">Question {cnt + 1}</div>
                    <Timer
                        minutes={0}
                        seconds={90}
                        signalTimerFinished={signalTimerFinished}
                        recordTime={recordTime}
                    />
                </div>
                <QuizQuestion
                    cnt={cnt}
                    answers={questions[cnt] && questions[cnt].answers}
                    questionTotal={questionTotal}
                    question={questions[cnt] && questions[cnt].question}
                    onSelectAnswer={this.nextAnswer}
                />
            </div>
        )
    }
}

export default Quiz
