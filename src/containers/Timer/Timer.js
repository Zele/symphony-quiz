// @flow
import * as React from 'react'
import classnames from 'classnames'

import './Timer.css'

type PropsType = {
  signalTimerFinished: Function,
  recordTime: Function,
  minutes: number,
  seconds: number,
}

type StateType = {
  minutesLeft: string,
  secondsLeft: string,
  millisLeft: number,
  percentage: number,
}

class Timer extends React.Component<PropsType, StateType> {
  static INTERVAL = 200

  constructor(props) {
    super(props)
    let { minutes, seconds } = this.props
    if (seconds > 59) {
      minutes += Math.floor(seconds / 60)
      seconds = seconds % 60
    }

    this.state = {
      minutesLeft: minutes >= 10 ? minutes.toString() : Timer.addLeadingZeros(minutes),
      secondsLeft: seconds >= 10 ? seconds.toString() : Timer.addLeadingZeros(seconds),
      millisLeft: (minutes * 60 + seconds) * 1000,
      millisTotal: (minutes * 60 + seconds) * 1000,
      percentage: 0
    }
  }

  componentDidMount() {
    let { millisLeft, millisTotal } = this.state

    this.interval = setInterval(() => {
      millisLeft -= Timer.INTERVAL

      if (millisLeft <= 0) {
        const { signalTimerFinished } = this.props

        signalTimerFinished()
        this.stop()
        return
      }

      const secondsLeft = Math.floor(millisLeft / 1000) % 60
      const minutesLeft = Math.floor(millisLeft / (1000 * 60)) % 60

      this.setState({
        secondsLeft: secondsLeft > 10 ?
          secondsLeft.toString() : Timer.addLeadingZeros(secondsLeft),
        minutesLeft: minutesLeft > 10 ? minutesLeft.toString() : Timer.addLeadingZeros(minutesLeft),
        millisLeft: millisLeft,
        percentage: 100 - Math.round(millisLeft * 100 / millisTotal)
      })
    }, Timer.INTERVAL)
  }

  componentWillUnmount() {
    this.stop()
  }

  stop() {
    const { recordTime } = this.props
    const { millisLeft, millisTotal } = this.state
    console.log('time recorded')
    recordTime(millisTotal - millisLeft)
    clearInterval(this.interval)
  }

  static addLeadingZeros(num) {
    let value = String(num)
    while (value.length < 2) {
      value = '0' + value
    }
    return value
  }

  render() {
    const { minutesLeft, secondsLeft, percentage } = this.state
    return (
      <div className='timer-container'>
        <div className={classnames({
          'timer': true,
          'pulseit': percentage >= 80
        })}>
          <span className='timer-minutes'>{minutesLeft}</span>
          :
          <span className={'timer-seconds'}>{secondsLeft}</span>
        </div>

        <div>
          <ProgressBar percentage={percentage} />
        </div>
      </div>
    )
  }
}

const ProgressBar = ({ percentage }: { percentage: number }) =>
  <div
    className={
      classnames({
        'progress-bar': true,
        'progress-bar-purple': percentage < 80,
        'progress-bar-red': percentage >= 80,
        'pulseit': true,
      })
    }
  >
    <Filler percentage={percentage}/>
  </div>

const Filler = ({ percentage }: { percentage: number }) =>
  <div
    className={
      classnames({
        'filler': true,
        'filler-purple': percentage < 80,
        'filler-red': percentage >= 80,
      })
    }
    style={{ width: `${percentage}%` }}
  />

export default Timer
