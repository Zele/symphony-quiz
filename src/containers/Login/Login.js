// @flow
import * as React from 'react'
import Button from '../../components/common/Button'

import './Login.css'

type PropsType = {
    onClick: Function
}

type StateType = {
    nickname: string,
    email: string
}

class Login extends React.Component<PropsType, StateType> {
    constructor(props) {
        super(props)
        this.state = {
            nickname: '',
            email: ''
        }
    }

    onChange = e => this.setState({ [e.target.name]: e.target.value })

    render() {
        const { onClick } = this.props
        const { nickname, email } = this.state
        return (
            <form
              className='login-form'
              onSubmit={() => onClick(nickname, email)}
            >
                <input
                    className='input common'
                    type='text'
                    name='nickname'
                    placeholder='nickname'
                    autoComplete='off'
                    required
                    onChange={this.onChange}
                />
                <input
                    className='input common'
                    type='text'
                    name='email'
                    placeholder='e-mail'
                    autoComplete='off'
                    required
                    onChange={this.onChange}
                />

                <Button
                    className='btn btn-orange common'
                    label='START QUIZ'
                />
            </form>
        )
    }
}

export default Login
