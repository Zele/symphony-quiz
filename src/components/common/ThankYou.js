// @flow
import * as React from 'react'
import Button from './Button'

import './ThankYou.css'

type PropsType = {
    onClick: Function,
    score: number,
    total: number,
    validateUserHasLoggedIn: Function,
}

const ThankYou = ({ onClick, score, total, validateUserHasLoggedIn }: PropsType) => {
    validateUserHasLoggedIn()

    return (
        <form className='ty-form'>
            <h1 className='ty-header'>Thank you for playing!</h1>
            <p className='ty-content'>
                You have answered correctly {score} out of {total} questions!
            </p>
            <p className='ty-content'>
                Be sure to check your e-mail for something special. :)
            </p>
            <div className='ty-button'>
                <Button
                    onClick={onClick}
                    label='GOT IT'
                />
            </div>
        </form>
    )
}

export default ThankYou
