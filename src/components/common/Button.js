// @flow

import * as React from 'react'

import './Button.css'

type PropsType = {
    onClick?: Function,
    label: string,
    type?: string
}

const Button = ({ label, onClick, type }: PropsType ) =>
    <button
        className='btn btn-orange common'
        onClick={onClick}
        type={type}
    >
        {label}
    </button>

Button.defaultValues = {
    type: 'submit',
    onClick: () => true
}

export default Button
