// @flow

import * as React from 'react'

import './Question.css'

export type PropsType = {
    content: string
}

const Question = ({ content }: PropsType) =>
    <h2 className='question'>{ content }</h2>

export default Question
