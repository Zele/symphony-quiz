// @flow
import * as React from 'react'

import './AnswerList.css'

export type PropsType = {
    children: Array<React.Element>
}

const AnswerList = ({ children }: PropsType) => {
    if (!children || !children.length) {
        return null
    }

    const halfLength = Math.ceil(children.length / 2)
    const leftHalf = children.slice(0, halfLength)
    const rightHalf = children.slice(halfLength, children.length)

    return (
        <table className='answer-list'>
            <tbody>
                <tr className='answer-row'>
                    {leftHalf}
                </tr>
                <tr className='answer-row'>
                    {rightHalf}
                </tr>
            </tbody>
        </table>
    )
}

export default AnswerList
