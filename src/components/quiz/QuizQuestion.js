// @flow
import * as React from 'react'
import Question, { PropsType as QuestionType } from './Question'
import AnswerList from './AnswerList'
import { AnswerType } from '../../containers/Quiz'
import AnswerOption from './AnswerOption'

import './QuizQuestion.css'

type PropsType = {
    question: QuestionType,
    answers: Array<AnswerType>,
    onSelectAnswer: Function
}

const QuizQuestion = ({ question, answers, onSelectAnswer }: PropsType) =>
    <div className='quiz'>
        <Question content={question} />
        <AnswerList>
            {
                answers && answers.map(answer =>
                    <AnswerOption
                        content={answer.content}
                        id={answer.id}
                        key={answer.id}
                        onAnswerSelected={onSelectAnswer}
                    />
                )
            }
        </AnswerList>
    </div>

export default QuizQuestion
