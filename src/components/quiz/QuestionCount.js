// @flow

import * as React from 'react'

type PropsType = {
    cnt: number,
    total: number
}

const QuestionCount = ({ cnt, total }: PropsType) => {
    return (
        <div className='question-count'>
            <p>
                Question <span>{cnt}</span> of <span>{total}</span>
            </p>
        </div>
    )
}

export default QuestionCount
