// @flow

import * as React from 'react'

import './AnswerOption.css'

type PropsType = {
    content: string,
    id: number,
    onAnswerSelected: Function
}

const AnswerOption = ({ content, id, onAnswerSelected }: PropsType) => {
    return (
        <td key={id} className='answer-option'>
            <button
                className='answer-button'
                id={id}
                value={content}
                onClick={() => onAnswerSelected(id)}
            >
                {content}
            </button>

        </td>
    )
}

export default AnswerOption
