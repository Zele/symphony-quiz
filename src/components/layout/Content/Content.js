// @flow

import React, { Fragment } from 'react'

import './Content.css'

type PropsType = {
    children: Array<React.Element>
}

const Content = ({ children }: PropsType) =>
    <Fragment>
        {children}
    </Fragment>

export default Content
