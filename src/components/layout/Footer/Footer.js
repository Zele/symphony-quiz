import * as React from 'react'

import './Footer.css'

const Footer = () =>
    <div className='text disable-select'>
        <span className='yellow'>you?</span>
        <span className='green'>&nbsp;+&nbsp;</span>
        <span className='cyan'>us</span>
        <span className='red'>&nbsp;=&nbsp;</span>
        <span className='purple'>symphony</span>
    </div>

export default Footer
