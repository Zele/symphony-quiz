import React from 'react'
import logoWhite from './symphony-logo-white.svg'

import './Sidebar.css'

const Sidebar = () =>
    <div className='sidebar'>
        <img
            src={logoWhite}
            className='sidebar-logo'
            alt='symphony.is'
        />
    </div>

export default Sidebar
