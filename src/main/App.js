import React, { Component } from 'react'
import { Route, withRouter } from 'react-router-dom'
import Sidebar from '../components/layout/Sidebar/Sidebar'
import Content from '../components/layout/Content/Content'
import Login from '../containers/Login/Login'
import Quiz from '../containers/Quiz'
import Footer from '../components/layout/Footer/Footer'
import ThankYou from '../components/common/ThankYou'

import './App.css'
import ScoreApi from '../api/ScoreApi'

const initialState = {
    correctAnswers: 0,
    timeUsedMs: 0,
    nickname: '',
    email: ''
}

const routes = {
    LOGIN: '/',
    QUIZ: '/quiz',
    THANKS: '/thanks',
}

const scoreApi = new ScoreApi()

class App extends Component {
    constructor(props) {
        super(props)
        this.state = initialState
    }

    resetState = () => this.setState(initialState)

    logIntoQuiz = (nickname, email) => {
        const { history } = this.props

        this.resetState()
        this.saveUserCreds(nickname, email)

        history.push(routes.QUIZ)
    }

    finishQuiz = timeUsedMs => {
        const { history } = this.props

        this.setState({ timeUsedMs })
        history.push(routes.THANKS)
    }

    incrementCorrectAnswerCount = () =>
        this.setState(prevState => ({ correctAnswers: prevState.correctAnswers + 1 }))

    recordTime = duration => this.setState({ timeUsedMs: duration })

    submitScore = () => {
        const { history } = this.props
        const { nickname, email, timeUsedMs, correctAnswers } = this.state
        const score = {
            nickname,
            email,
            timeUsedMs,
            correctAnswers
        }

        scoreApi.submitScore(score)

        this.resetState()
        history.push('/')
    }

    saveUserCreds = (nickname, email) => this.setState({nickname, email})

    validateUserHasLoggedIn = () => {
        const { history } = this.props
        const { nickname, email } = this.state

        if (!nickname || !email) {
            history.push(routes.LOGIN)
        }
    }

    render() {
        const { correctAnswers } = this.state
        const total = process.env.REACT_APP_QUESTION_NUM
        return (
            <div className='App-container'>
                <div className='App-center'>
                    <div className='App-content'>
                        <Content>
                            <Route
                                exact path={routes.LOGIN}
                                render={props => <Login {...props} onClick={this.logIntoQuiz} /> } />
                            <Route
                                exact path={routes.QUIZ}
                                render={props =>
                                    <Quiz
                                        {...props}
                                        signalTimerFinished={this.finishQuiz}
                                        incrementCorrectAnswerCount={this.incrementCorrectAnswerCount}
                                        recordTime={this.recordTime}
                                        validateUserHasLoggedIn={this.validateUserHasLoggedIn}
                                    />
                                }
                            />
                            <Route
                                exact path={routes.THANKS}
                                component={props =>
                                    <ThankYou
                                        {...props}
                                        onClick={this.submitScore}
                                        validateUserHasLoggedIn={this.validateUserHasLoggedIn}
                                        score={correctAnswers}
                                        total={total}
                                    />
                                }
                            />
                        </Content>
                    </div>
                    <div className='App-footer'>
                        <Footer />
                    </div>
                </div>
                <div className='App-sidebar'>
                    <Sidebar />
                </div>
            </div>
        )
  }
}

export default withRouter(App)
